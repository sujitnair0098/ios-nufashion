//
//  NewReviewViewController.m
//  FTV
//
//  Created by HelixTech-Admin on 24/08/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import "NewReviewViewController.h"

@interface NewReviewViewController (){
    CGSize keyboardSize;
    CGFloat duration;
}
@property (strong, nonatomic) IBOutlet UITextField *reviewTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *cameraBtn;
@property (strong, nonatomic) IBOutlet UITextField *reviewDescription;
@property (strong, nonatomic) IBOutlet UILabel *reviewByLabel;
@property (strong, nonatomic) IBOutlet UIButton *firstStar;
@property (strong, nonatomic) IBOutlet UIButton *secondStar;
@property (strong, nonatomic) IBOutlet UIButton *thirdStar;
@property (strong, nonatomic) IBOutlet UIButton *fourthStar;
@property (strong, nonatomic) IBOutlet UIButton *fifthStar;
@property NSString *reviewStars;
@property NSData *imageDetails;
@property NSData *imgData;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property NSURLSessionDataTask *dataTask;

@end

@implementation NewReviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cameraBtn.layer.cornerRadius = 25;
    self.cameraBtn.clipsToBounds = YES;
    self.cameraBtn.layer.borderWidth = 1.0f;
    self.cameraBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.cameraBtn.layer.shadowOpacity = 0.7;
    self.cameraBtn.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.cameraBtn.layer.shadowOffset = CGSizeMake(3, 3);
    self.cameraBtn.layer.shadowRadius = 4.0f;
    
    self.reviewDescription.delegate = self;
    
    
    [self.reviewByLabel setText:[NSString stringWithFormat:@"Reviewed By %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_name"]]];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect frame = self.view.frame;
    frame.origin.y = -keyboardSize.height;
    [UIView animateWithDuration:duration animations:^{
        self.view.frame = frame;
    }];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect frame = self.view.frame;
    frame.origin.y = self.view.window.frame.origin.y;
    [UIView animateWithDuration:duration animations:^{
        self.view.frame = frame;
    }];
}


- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    
    keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    CGRect frame = self.view.frame;
    frame.origin.y = -keyboardSize.height;
    
}


- (void)keyboardWillHide:(NSNotification *)notification
{
    
    
    NSDictionary *userInfo = [notification userInfo];
    [UIView animateWithDuration:[userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]
                          delay:0.f
                        options:[[notification userInfo][UIKeyboardAnimationCurveUserInfoKey] intValue] << 16
                     animations:^{
                         
                     } completion:^(BOOL finished) {
                         
                     }];
}


- (IBAction)cameraBtnActn:(UIButton *)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Select Image" message:@"Select Image" preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self camera];
    }];
    
    UIAlertAction *gallery = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self gallery];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:camera];
    [alert addAction:gallery];
    [alert addAction:cancel];
//    [self presentViewController:alert animated:YES completion:nil];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:alert];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.width/2, 100, 100) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)camera
{
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
    }else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Camera Is not Available" message:@"Its a Simulator" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];

    }
}


-(void)gallery
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    self.imageDetails = UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerOriginalImage"], 1);
    UIImage *img = [[UIImage alloc]initWithData:_imageDetails];
    
    [self.imageView setImage:img];
    
    self.imgData = [NSData dataWithData:UIImagePNGRepresentation(img)];
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}

#pragma mark - Seding Profile Picture To Backend

-(void)updatingUserProfilePicandCustomerIdis:(NSString *)customerId andImageURL:(NSData *)imageData
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:customerId forKey:@"customerId"];
    
    NSLog(@"default Value is %@",[defaults objectForKey:@"customerId"]);
    
    NSString *urlString = [NSString stringWithFormat:@"http://httest.in/ftvws/RUC_FileUpload/customer_id/%@/status/1",customerId];
    NSLog(@"image Url %@",urlString);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"_187934598797439873422234";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"picture\"; filename=\"%@.png\"\r\n", [[NSUserDefaults standardUserDefaults] objectForKey:@"user_name"]] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imageData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [request setHTTPBody:body];
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
    
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    _dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (!error)
        {
            NSMutableDictionary *dataDic2 = [[NSMutableDictionary alloc]init];
            dataDic2 = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"data in dictionary is %@",dataDic2);
            
            NSString *rspnV = [[dataDic2 objectForKey:@"response"] stringValue];
            NSLog(@"Value in response Value is %@",rspnV);
            
            if ([rspnV isEqualToString:@"1"])
            {
                NSString *imgStr = [dataDic2 objectForKey:@"image_url"];
                NSURL *imgUrl = [NSURL URLWithString:imgStr];
                NSData *imgData = [NSData dataWithContentsOfURL:imgUrl];
                [[NSUserDefaults standardUserDefaults]setObject:imgData forKey:@"image"];
            }
            
        }else{
            NSLog(@"Error Description is %@",error.description);
        }
    }];
    [_dataTask resume];
    
}



- (IBAction)singleStar:(UIButton *)sender {
    

    [self.firstStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
    self.reviewStars = @"1";
    
    static int count = 0;
    count ++;
    if(count % 2 != 0){
        //Set title 1
        NSLog(@"where count modulus 2 not equal i value is %i",count);
        [self.firstStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
        self.reviewStars = @"1";
        
    }else{
        //Set title 2
        NSLog(@"where count modulus 2 equal i value is %i",count);
//        UIImage *image = [UIImage imageNamed:@"star.png"];
//        
//        if ([self.secondStar.currentImage isEqual:image])
//        {
//            
//        }
//        
        [self.firstStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        [self.secondStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        [self.thirdStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        [self.fourthStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        [self.fifthStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        self.reviewStars = @"0";
    }
//    count ++;
    
}
- (IBAction)twoStars:(UIButton *)sender {
    
    [self.firstStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
    [self.secondStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
    self.reviewStars = @"2";
    
    static int count = 0;
    count ++;
    if(count % 2 != 0){
        //Set title 1
        NSLog(@"where count modulus 2 not equal i value is %i",count);
        [self.firstStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
        [self.secondStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
        self.reviewStars = @"2";
        
    }else{
        //Set title 2
        NSLog(@"where count modulus 2 equal i value is %i",count);
        
        [self.secondStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        [self.thirdStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        [self.fourthStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        [self.fifthStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        self.reviewStars = @"1";
    }
//    count ++;
    
    
}
- (IBAction)threeStars:(UIButton *)sender {
    
    [self.firstStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
    [self.secondStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
    [self.thirdStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
    
    self.reviewStars = @"3";
    static int count = 0;
    count ++;
    if(count % 2 != 0){
        //Set title 1
        NSLog(@"where count modulus 2 not equal i value is %i",count);
        [self.firstStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
        [self.secondStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
        [self.thirdStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
        self.reviewStars = @"3";
        
    }else{
        //Set title 2
        NSLog(@"where count modulus 2 equal i value is %i",count);
//        [self.firstStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
//        [self.secondStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        [self.thirdStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        [self.fourthStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        [self.fifthStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        self.reviewStars = @"2";
    }
//    count ++;
}
- (IBAction)fourStars:(UIButton *)sender {
    [self.firstStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
    [self.secondStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
    [self.thirdStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
    [self.fourthStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
    self.reviewStars = @"4";
    static int count = 0;
    count ++;
    if(count % 2 != 0){
        //Set title 1
        NSLog(@"where count modulus 2 not equal i value is %i",count);
        [self.firstStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
        [self.secondStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
        [self.thirdStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
        [self.fourthStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
        self.reviewStars = @"4";
        
    }else{
        //Set title 2
        NSLog(@"where count modulus 2 equal i value is %i",count);
        [self.fourthStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        [self.fifthStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        self.reviewStars = @"3";
    }
//    count ++;
}
- (IBAction)fiveStars:(UIButton *)sender {
    [self.firstStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
    [self.secondStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
    [self.thirdStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
    [self.fourthStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
    [self.fifthStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
    self.reviewStars = @"5";
    static int count = 0;
    count ++;
    if(count % 2 != 0){
        //Set title 1
        NSLog(@"where count modulus 2 not equal i value is %i",count);
        [self.firstStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
        [self.secondStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
        [self.thirdStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
        [self.fourthStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
        [self.fifthStar setImage:[UIImage imageNamed:@"star (1).png"] forState:UIControlStateNormal];
        self.reviewStars = @"5";
        
    }else{
        //Set title 2
        NSLog(@"where count modulus 2 equal i value is %i",count);
        
//        [self.fourthStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        [self.fifthStar setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
        self.reviewStars = @"4";
    }
//    count ++;
    
    
}
- (IBAction)submitBtn:(UIButton *)sender {
    
//    http://httest.in/ftvws/RUC_ReviewInsert/designer_id/46/customer_id/82/review_title/Excellent%20One/review_description/Worth%20every%20penny%20spent/review_imageurl/asfsfaas/star_rating/5
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Submit" message:@"Do you want to SUBMIT review?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if ((![self.reviewDescription.text isEqualToString:@""])&&(![self.reviewTitle.text isEqualToString:@""])&&(![self.imgData isEqual:nil]))
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self GettingData];
                [self updatingUserProfilePicandCustomerIdis:[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_id"] andImageURL:self.imgData];
                
            });
            
        }
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
//    [self presentViewController:alert animated:YES completion:nil];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:alert];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.width/2, 100, 100) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    

}

-(void)GettingData
{
//    NSString *str = [NSString stringWithFormat:@"http://httest.in/ftvws/RUC_ReviewInsert/designer_id/46/customer_id/%@/review_title/%@/review_description/%@/review_imageurl/%@/star_rating/%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_id"],self.reviewTitle.text,self.reviewDescription.text,[[NSUserDefaults standardUserDefaults] objectForKey:@"imageUrl"],self.reviewStars];

    NSString *str = [NSString stringWithFormat:@"http://httest.in/ftvws/RUC_ReviewInsert/designer_id/52/customer_id/%@/review_title/%@/review_description/%@/review_imageurl/%@/star_rating/%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_id"],self.reviewTitle.text,self.reviewDescription.text,[[NSUserDefaults standardUserDefaults] objectForKey:@"imageUrl"],self.reviewStars];
    
    NSLog(@"URL String is %@",str);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]];
    NSURLSessionConfiguration *cnfgrtion = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:cnfgrtion];
    
    _dataTask = [[NSURLSessionDataTask alloc]init];
    
    
    _dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (!error)
        {
            NSMutableDictionary *dataDic2 = [[NSMutableDictionary alloc]init];
            dataDic2 = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"data in whats New dictionary is %@",dataDic2);
            [self changingTheData:dataDic2];
            dispatch_async(dispatch_get_main_queue(), ^{
            });
        }
        
        
    }];
    
    [_dataTask resume];
}

-(void)changingTheData:(NSMutableDictionary *)dic
{
    NSLog(@"Data in Dictionary is %@",dic);
}


@end
