//
//  ProductImgVController.h
//  FTV
//
//  Created by HelixTech-Admin on 10/08/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductImgVController : UIViewController<UIScrollViewDelegate>

@property UIImage *image;

@end
