//
//  NewReviewViewController.h
//  FTV
//
//  Created by HelixTech-Admin on 24/08/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewReviewViewController : UIViewController<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@end
