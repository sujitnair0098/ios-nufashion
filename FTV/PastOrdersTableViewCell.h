//
//  PastOrdersTableViewCell.h
//  NuFashion
//
//  Created by HelixTech-Admin on 25/08/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PastOrdersTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) IBOutlet UILabel *priceValue;
@property (strong, nonatomic) IBOutlet UILabel *orderedDate;

@end
