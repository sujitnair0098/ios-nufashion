//
//  Setting1TableViewCell.h
//  FTV
//
//  Created by HelixTech-Admin on 09/08/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Setting1TableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *itemNameLbl;

@end
