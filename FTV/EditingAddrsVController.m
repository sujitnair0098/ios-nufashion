//
//  EditingAddrsVController.m
//  FTV
//
//  Created by HelixTech-Admin on 10/08/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import "EditingAddrsVController.h"

@interface EditingAddrsVController ()
@property (strong, nonatomic) IBOutlet UITextField *streetTF;
@property (strong, nonatomic) IBOutlet UITextField *cityTF;
@property (strong, nonatomic) IBOutlet UITextField *stateTF;
@property (strong, nonatomic) IBOutlet UITextField *countryTF;
@property (strong, nonatomic) IBOutlet UITextField *pincodeTF;

@property CLLocation *locationData;
@property CLGeocoder *geocoder;
@property CLPlacemark *placemark;
@property UIActivityIndicatorView *activityView;
@property NSURLSessionDataTask *dataTask;

@property CLLocationManager *locationManager;

@end

@implementation EditingAddrsVController

- (void)viewDidLoad {
    [super viewDidLoad];
    _stateTF.delegate = self;
    _streetTF.delegate = self;
    _countryTF.delegate = self;
    _pincodeTF.delegate = self;
    _cityTF.delegate = self;
}

- (IBAction)backBtn:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)gettingUserLocation:(UIButton *)sender {
    _activityView = [[UIActivityIndicatorView alloc]
                     initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    _activityView.center=self.view.center;
    [_activityView startAnimating];
    [self.view addSubview:_activityView];
    self.locationManager.delegate = self;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ((textField == self.countryTF) || (textField == self.stateTF) || (textField == self.cityTF))
    {
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
        }
    }else if (textField == self.pincodeTF)
    {
        if ((self.pincodeTF.text.length == 10) && (range.length == 0))
        {
            return NO;
        }else
        {
            return YES;
        }
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if ((textField == self.cityTF) || (textField == self.stateTF) || (textField == self.countryTF))
    {
        [textField becomeFirstResponder];
    }else if (textField == self.pincodeTF)
    {
        [textField resignFirstResponder];
    }
    return  YES;
}


-(void)viewWillAppear:(BOOL)animated
{
    [self.locationManager startUpdatingLocation];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
    self.locationData = [locations lastObject];
    
    NSLog(@"%.8f", self.locationData.coordinate.latitude);
    
    
    [_geocoder reverseGeocodeLocation:self.locationData completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0) {
            _placemark = [placemarks lastObject];
            
            
            
            NSLog(@"Placeholder city is %@ postal Value is %@ adminstritative area is %@ and Country is %@",_placemark.thoroughfare,_placemark.postalCode,_placemark.administrativeArea,_placemark.country);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                self.cityTF.text = [NSString stringWithFormat:@"%@",_placemark.locality];
                self.stateTF.text = [NSString stringWithFormat:@"%@",_placemark.administrativeArea];
                self.pincodeTF.text = [NSString stringWithFormat:@"%@",_placemark.postalCode];
                self.streetTF.text = [NSString stringWithFormat:@"%@",_placemark.thoroughfare];
                self.countryTF.text = [NSString stringWithFormat:@"%@",_placemark.country];
                
                [_activityView stopAnimating];
                [self.locationManager stopUpdatingLocation];
            });
            
        } else {
            NSLog(@"%@", error.debugDescription);
        }
        
        
    } ];
}

- (IBAction)submitBtn:(UIButton *)sender {
    
    NSString *str = [NSString stringWithFormat:@"http://httest.in/ftvws/RUC_ChangeAddress/customer_id/%@/permanent/0/street/%@/city/%@/state/%@/country/%@/pincode/%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_id"],_streetTF.text,_cityTF.text,_stateTF.text,_countryTF.text,_pincodeTF.text];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]];
    NSURLSessionConfiguration *cnfgrtion = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:cnfgrtion];
    
    _dataTask = [[NSURLSessionDataTask alloc]init];
    
    
    _dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error)
        {
            NSMutableDictionary *dataDic2 = [[NSMutableDictionary alloc]init];
            dataDic2 = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            NSLog(@"data in dictionary is %@",dataDic2);
            
            NSString *response1 =  [NSString stringWithFormat:@"%@",[dataDic2 objectForKey:@"response"]];
            
            if (![response1 isEqualToString:@"0"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self dismissViewControllerAnimated:YES completion:nil];
                });
            }else
            {
                //Do nothing
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                });
                
            }
        }else{
            NSLog(@"Error Occured");
        }
        
        
    }];
    
    [_dataTask resume];
    
}

@end
