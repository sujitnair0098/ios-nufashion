//
//  CartDetailsVController.m
//  FTV
//
//  Created by HelixTech-Admin on 19/08/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import "CartDetailsVController.h"

@interface CartDetailsVController ()

@property (strong, nonatomic) IBOutlet UILabel *totalPriceLbl;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableArray *imagesArray;
@property NSMutableArray *productsNameArray;
@property NSMutableArray *productIdArray;
@property NSMutableArray *priceArra;
@property NSMutableArray *offerPercentageArray;
@property NSMutableArray *advacePercentagePayArray;
@property NSMutableArray *priceAfterAdvncePaymntArr;
@property NSString *stepperIncreasedValue;
@property NSURLSessionDataTask *dataTask;
@property NSString *stepperValue;
@property NSString *totalPriceStr;
@property (strong, nonatomic) IBOutlet UIButton *checkoutBtn;
@property (strong, nonatomic) IBOutlet UILabel *paybleAmntLbl;
@property (strong, nonatomic) IBOutlet UIView *checkOutView;
@property UIActivityIndicatorView *spinner;

-(void)closeBtnActn:(UIButton *)sender event:(id)event;
-(void)stepper:(UIStepper *)sender event:(id)event;

@end

@implementation CartDetailsVController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self gettingAllDetailsOfProduct];
    self.tabBarController.tabBar.hidden = YES;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    _spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _spinner.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    [_spinner setHidden:NO];
    [_spinner startAnimating];
    [self.view addSubview:_spinner];
    [self.view bringSubviewToFront:_spinner];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(success:) name:@"payment_success_notifications" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(failure:) name:@"payment_failure_notifications" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancel:) name:@"payu_notifications" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataReceived:) name:@"passData" object:nil];
}

- (void) success:(NSDictionary *)info{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void) failure:(NSDictionary *)info{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void) cancel:(NSDictionary *)info{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)dataReceived:(NSNotification *)noti
{
    NSLog(@"dataReceived from surl/furl:%@", noti.object);
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)gettingAllDetailsOfProduct
{
    _imagesArray = [[NSMutableArray alloc]init];
    _productIdArray = [[NSMutableArray alloc]init];
    _productsNameArray = [[NSMutableArray alloc]init];
    _priceArra = [[NSMutableArray alloc]init];
    _offerPercentageArray = [[NSMutableArray alloc]init];
    _advacePercentagePayArray = [[NSMutableArray alloc]init];
    _priceAfterAdvncePaymntArr = [[NSMutableArray alloc]init];
    
    NSString *str = [NSString stringWithFormat:@"http://httest.in/ftvws/RUC_CartItems/customer_id/%@/pageno/0/designer_id/52",[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_id"]];
    
    NSLog(@"URL String is %@",str);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]];
    NSURLSessionConfiguration *cnfgrtion = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:cnfgrtion];
    
    _dataTask = [[NSURLSessionDataTask alloc]init];
    
    _dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSMutableDictionary *dataDic2 = [[NSMutableDictionary alloc]init];
        dataDic2 = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"data in dictionary is %@",dataDic2);
        
        if (!error)
        {
            
            //            Adding Product Details To Our Local objects
            
            [self ProductDetailsAre:dataDic2];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                self.tableView.hidden = NO;
                _totalPriceLbl.text = [NSString stringWithFormat:@"₹ %@",_totalPriceStr];
                [self.tableView reloadData];
                self.checkoutBtn.hidden = NO;
                self.checkOutView.hidden = NO;
                self.paybleAmntLbl.hidden = NO;
                self.totalPriceLbl.hidden = NO;
                [_spinner stopAnimating];
                [_spinner setHidden:YES];
                
            });
            
        }else{
            
        }
        
    }];
    
    [_dataTask resume];
    
}

-(void)closeBtnActn:(UIButton *)sender event:(id)event
{
    
    NSLog(@"Close Button is Pressed");
    id superView = sender.superview;
    while (superView && ![superView isKindOfClass:[CartTableViewCell class]]) {
        superView = [superView superview];
    }
    
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    
    CGPoint currentTouchPosition = [touch locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:currentTouchPosition];
    
    NSString *str = [NSString stringWithFormat:@"http://httest.in/ftvws/RUC_ToggleCart/customer_id/%@/product_id/%@/qty/1/status/0/designer_id/52",[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_id"],[_productIdArray objectAtIndex:indexPath.item]];
    
    NSLog(@"URL String is %@",str);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]];
    NSURLSessionConfiguration *cnfgrtion = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:cnfgrtion];
    
    _dataTask = [[NSURLSessionDataTask alloc]init];
    
    _dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSMutableDictionary *dataDic2 = [[NSMutableDictionary alloc]init];
        dataDic2 = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"data in dictionary is %@",dataDic2);
        
        if (!error)
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self gettingAllDetailsOfProduct];
                if (_productIdArray.count == 0)
                {
                }else
                {
                        [self.tableView reloadData];
                    _totalPriceLbl.text = _totalPriceStr;
                }
                
            });
            
        }else{
            
        }
        
    }];
    
    [_dataTask resume];

}

-(void)stepper:(UIStepper *)sender event:(id)event
{
    
    double value = [sender value];
    
    int value2 = (int)value;
    
    self.stepperValue = [NSString stringWithFormat:@"%i",value2];
    
    [self.tableView reloadData];
    
}

-(void)ProductDetailsAre:(NSMutableDictionary *)dic
{
    
    int sum = 0;
    for (int i =0 ; i < [[dic objectForKey:@"data"] count]; i++)
    {
        [_offerPercentageArray addObject:[[[dic objectForKey:@"data"] objectAtIndex:i] objectForKey:@"offer_percentage"]];
        [_advacePercentagePayArray addObject:[[[dic objectForKey:@"data"] objectAtIndex:i] objectForKey:@"advance_amount_percentage"]];
        [_priceArra addObject:[[[dic objectForKey:@"data"] objectAtIndex:i] objectForKey:@"price"]];
        [_productsNameArray addObject:[[[dic objectForKey:@"data"] objectAtIndex:i] objectForKey:@"product_name"]];
        [_productIdArray addObject:[[[dic objectForKey:@"data"] objectAtIndex:i] objectForKey:@"product_id"]];
        [_imagesArray addObject:[[[dic objectForKey:@"data"] objectAtIndex:i] objectForKey:@"image1"]];
    }
    
    for (int i = 0; i<_advacePercentagePayArray.count; i++)
    {
        int advancePayment = [[_advacePercentagePayArray objectAtIndex:i] intValue];
        int priceV = [[_priceArra objectAtIndex:i] intValue];
        int afterOffer = (priceV - (advancePayment*priceV)/100);
        sum = sum +afterOffer;
        
        if (!(advancePayment == 100))
        {
            [_priceAfterAdvncePaymntArr addObject:[NSString stringWithFormat:@"%i",afterOffer]];
        }
        else
        {
            [_priceAfterAdvncePaymntArr addObject:[NSString stringWithFormat:@"%i",priceV]];
            
        }
    }
    
    for (int i =0 ; i< _priceAfterAdvncePaymntArr.count; i++)
    {
        int item = [[_priceAfterAdvncePaymntArr objectAtIndex:i] intValue];
        sum = sum +item;
    }
    _totalPriceStr = [NSString stringWithFormat:@"%i",sum];
    
    NSLog(@"Data in PriceAfterAdvncePayment is %@",_priceAfterAdvncePaymntArr);
    
    if (_imagesArray.count >= 3)
    {
        self.tableView.scrollEnabled = YES;
    }else
    {
        self.tableView.scrollEnabled = NO;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _imagesArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CartTableViewCell"];
    
    if (cell == nil)
    {
        NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"CartTableViewCell" owner:self options:nil];
        cell = [arr lastObject];
    }
    
    cell.productName.text = [_productsNameArray objectAtIndex:indexPath.item];
    
    cell.priceLbl.text = [NSString stringWithFormat:@"₹ %@",[_priceAfterAdvncePaymntArr objectAtIndex:indexPath.item]];
    
    cell.backGroundView.clipsToBounds = YES;
    cell.backGroundView.layer.cornerRadius = 2.0f;
    cell.backGroundView.layer.borderWidth = 1.5f;
    cell.backGroundView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.backGroundView.layer.shadowRadius = 2.0f;
    cell.backGroundView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    cell.backGroundView.layer.shadowOffset = CGSizeMake(3, 3);
    
    [cell.stepper addTarget:self action:@selector(stepper:event:) forControlEvents:UIControlEventValueChanged];
    
    cell.quantityLbl.text = [NSString stringWithFormat:@"1"];
    
    if ([self.stepperValue isEqualToString:@""])
    {
        cell.quantityLbl.text = [NSString stringWithFormat:@"1"];
    }else{
     
        cell.quantityLbl.text = self.stepperValue;
        
    }
    
    
    NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[_imagesArray objectAtIndex:indexPath.item]]];
    UIImage *image = [UIImage imageWithData:imgData];
    
    UIImageView *imagView = [[UIImageView alloc]initWithFrame:CGRectMake(8, 35, 93, 117)];
    [imagView setImage:image];
    [cell.backGroundView addSubview:imagView];
    
    [cell.closeBtn addTarget:self action:@selector(closeBtnActn:event:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 203;
}

- (IBAction)checkOutBtn:(UIButton *)sender {
    
    NSString *msg =[NSString stringWithFormat:@"Proceed To Pay %@",self.totalPriceLbl.text];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Proceed" message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *Ok =[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        PayUPaymentOptionsViewController *paymentOptionsVC = nil;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            CGSize result = [[UIScreen mainScreen] bounds].size;
            if(result.height == 480)
            {
                paymentOptionsVC = [[PayUPaymentOptionsViewController alloc] initWithNibName:@"AllPaymentOprionsView" bundle:nil];
            }
            else
            {
                paymentOptionsVC = [[PayUPaymentOptionsViewController alloc] initWithNibName:@"PayUPaymentOptionsViewController" bundle:nil];
            }
        }
        //Pass the parameters in paramDict in Key-Value pair as mentioned
        
        NSString *price = [NSString stringWithFormat:@"%@",self.totalPriceLbl.text];
        
        NSString *firstName = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_name"]];
        NSString *productInfo = [_productsNameArray componentsJoinedByString:@","];
        NSString *emailId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"email_id"]];
        
        
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          productInfo,@"productinfo",
                                          firstName,@"firstname",
                                          price,@"amount",
                                          emailId,@"email",
                                          @"9876543210", @"phone",
                                          @"httpss://dl.dropboxusercontent.com/s/y911hgtgdkkiy0w/success_iOS.html",@"surl",
                                          @"httpss://dl.dropboxusercontent.com/s/h6m11xr93mxhfvf/Failure_iOS.html",@"furl",
                                          // _txnID is your Transaction ID set by you inside the app
                                          @12345678,@"txnid",
                                          @"ra:ra",@"user_credentials",
                                          @"offertest@1411",@"offer_key",
                                          @"u1",@"udf1",
                                          @"u2",@"udf2",
                                          @"u3",@"udf3",
                                          @"u4",@"udf4",
                                          @"u5",@"udf5"
                                          ,nil];
        paymentOptionsVC.parameterDict = paramDict;
        paymentOptionsVC.callBackDelegate = self;
        paymentOptionsVC.totalAmount  = 1100.6;
        paymentOptionsVC.appTitle     = @"PayU test App";
        if(_hashDict)
            paymentOptionsVC.allHashDict = _hashDict;
        _hashDict = nil;
        [self.navigationController pushViewController:paymentOptionsVC animated:YES];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:Ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
