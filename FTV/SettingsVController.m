//
//  SettingsVController.m
//  FTV
//
//  Created by HelixTech-Admin on 09/08/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import "SettingsVController.h"

@interface SettingsVController ()

@property (strong, nonatomic) IBOutlet UIImageView *userImageVIew;
@property (strong, nonatomic) IBOutlet UIButton *roundCamBtn;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property UIImagePickerController *imagePicker;
@property NSData *imageDetails;
@property NSData *imgData;
@property NSURLSessionDataTask *dataTask;

@end

@implementation SettingsVController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _roundCamBtn.layer.cornerRadius = 25;
    _roundCamBtn.clipsToBounds = YES;
    _roundCamBtn.layer.borderWidth = 1.0f;
    _roundCamBtn.layer.shadowOffset = CGSizeMake(3, 3);
    _roundCamBtn.layer.shadowRadius = 2.0f;
    _roundCamBtn.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    
    
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"image"] isEqual:nil]) {
        
        _userImageVIew.layer.cornerRadius = 75;
        _userImageVIew.clipsToBounds = YES;
        _userImageVIew.layer.borderWidth = 1.0f;
        _userImageVIew.layer.borderColor = [UIColor blackColor].CGColor;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //                NSURL *imgUrl = [NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"image"]];
            NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"image"];
            UIImage *img = [UIImage imageWithData:data];
            dispatch_async(dispatch_get_main_queue(), ^{
                [_userImageVIew setImage:img];
            });
        });
    }
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.scrollEnabled = NO;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger value;
    
    if (section == 0)
    {
        value = 1;
    }else if (section == 1)
    {
        value = 3;
    }
    
    return value;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        Settings3TViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Settings3TViewCell"];
        if (cell == nil)
        {
            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"Settings3TViewCell" owner:self options:nil];
            cell = [arr objectAtIndex:0];
        }
        NSLog(@"Data in userDefaults is %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"user_name"]);
        
        [cell.userName setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"user_name"]];
        return cell;
    }else if (indexPath.section == 1)
    {
        if (indexPath.item <= 1)
        {
            Setting1TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Setting1TableViewCell"];
            if (cell == nil)
            {
                NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"Setting1TableViewCell" owner:self options:nil];
                cell = [arr objectAtIndex:0];
            }
            if (indexPath.item == 0)
            {
                [cell.title setText:@"Email"];
                [cell.itemNameLbl setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"email_id"]];
            }else if (indexPath.item == 1)
            {
                [cell.title setText:@"Phone Number"];
                
                if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"mobile"] isEqualToString:@""])
                {
                    [cell.itemNameLbl setText:@"Click TO Set Phone Number"];
                }else{
                    [cell.itemNameLbl setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"mobile"]];
                }
            }
            return cell;
        }else if (indexPath.item == 2)
        {
             Setting2TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Setting2TableViewCell"];
            if (cell == nil)
            {
                NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"Setting2TableViewCell" owner:self options:nil];
                cell = [arr objectAtIndex:0];
            }
            [cell.stateName setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"state"]];
            [cell.streetName setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"street"]];
            [cell.cityName setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"city"]];
            [cell.countryName setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"country"]];
            [cell.pincode setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"pincode"]];
            return cell;
        }
    }
    
    return nil;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat value;
    
    if (indexPath.section == 0)
    {
        value = 62;
    }else if (indexPath.section == 1)
    {
        if (indexPath.item <= 1)
        {
            value = 78 ;
        }else{
            value = 154;
        }
    }
    
    return value;
}

- (IBAction)roundCamBtn:(UIButton *)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Select Image" message:@"Select Image" preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self camera];
    }];
    
    UIAlertAction *gallery = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self gallery];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:camera];
    [alert addAction:gallery];
    [alert addAction:cancel];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        // Change Rect to position Popover
        
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:alert];
        
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.width/2, 100, 100) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

- (IBAction)backBtn:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)camera
{
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
    }

}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}



-(void)gallery
{
    _imagePicker = [[UIImagePickerController alloc]init];
    _imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    _imagePicker.delegate = self;
    [self presentViewController:_imagePicker animated:YES completion:nil];
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    _imageDetails = UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerOriginalImage"], 1);
    UIImage *img = [[UIImage alloc]initWithData:_imageDetails];
    
    [_userImageVIew setImage:img];
    
    _imgData = [NSData dataWithData:UIImagePNGRepresentation(img)];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self updatingUserProfilePicandCustomerIdis:[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_id"] andImageURL:_imgData];
    }];
    
}

#pragma mark - Seding Profile Picture To Backend

-(void)updatingUserProfilePicandCustomerIdis:(NSString *)customerId andImageURL:(NSData *)imageData
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:customerId forKey:@"customerId"];
    
    NSLog(@"default Value is %@",[defaults objectForKey:@"customerId"]);
    
    NSString *urlString = [NSString stringWithFormat:@"http://httest.in/ftvws/RUC_FileUpload/customer_id/%@/status/1",customerId];
    NSLog(@"image Url %@",urlString);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"_187934598797439873422234";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"picture\"; filename=\"%@.png\"\r\n", [[NSUserDefaults standardUserDefaults] objectForKey:@"user_name"]] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:imageData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [request setHTTPBody:body];
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
    
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    _dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (!error)
        {
            NSMutableDictionary *dataDic2 = [[NSMutableDictionary alloc]init];
            dataDic2 = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"data in dictionary is %@",dataDic2);
            
            NSString *rspnV = [[dataDic2 objectForKey:@"response"] stringValue];
            NSLog(@"Value in response Value is %@",rspnV);
            
            if ([rspnV isEqualToString:@"1"])
            {
                NSString *imgStr = [dataDic2 objectForKey:@"image_url"];
                NSURL *imgUrl = [NSURL URLWithString:imgStr];
                NSData *imgData = [NSData dataWithContentsOfURL:imgUrl];
                [[NSUserDefaults standardUserDefaults]setObject:imgData forKey:@"image"];
            }
            
        }else{
            NSLog(@"Error Description is %@",error.description);
        }
    }];
    [_dataTask resume];
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EditingNavVController *nextView = [self.storyboard instantiateViewControllerWithIdentifier:@"EditingNavVController"];
    if (indexPath.section == 0)
    {
        nextView.selectedName = @"Enter Your Name";
        
        [self presentViewController:nextView animated:YES completion:^{
            [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        }];
    }else if (indexPath.section == 1)
    {
        if (indexPath.item == 0)
        {
                nextView.selectedName = @"Enter Yur Email";
            [self presentViewController:nextView animated:YES completion:^{
                [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
            }];
        }else if (indexPath.item == 1)
        {
            nextView.selectedName = @"Enter Your Number";
            [self presentViewController:nextView animated:YES completion:^{
                [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
            }];
        }else if (indexPath.item == 2)
        {
            EditingAddrsVController *addrsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EditingAddrsVController"];
            [self presentViewController:addrsVC animated:YES completion:^{
                [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
            }];
        }
    }
    
}


@end
