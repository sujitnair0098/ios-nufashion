//
//  EditingViewController.m
//  FTV
//
//  Created by HelixTech-Admin on 10/08/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import "EditingViewController.h"

@interface EditingViewController ()
@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UINavigationItem *navItem;

@property NSURLSessionDataTask *dataTask;

@end

@implementation EditingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_navItem setTitle:_selectedCellName];
    if ([_selectedCellName isEqualToString:@"Enter Your Name"])
    {
        [_textField setKeyboardType:UIKeyboardTypeNamePhonePad];
        [_textField becomeFirstResponder];
    }else if ([_selectedCellName isEqualToString:@"Enter Yur Email"])
    {
        [_textField setKeyboardType:UIKeyboardTypeEmailAddress];
        [_textField becomeFirstResponder];
    }else if ([_selectedCellName isEqualToString:@"Enter Your Number"])
    {
        [_textField setKeyboardType:UIKeyboardTypePhonePad];
        [_textField becomeFirstResponder];
    }
    
}

- (IBAction)saveBtn:(UIBarButtonItem *)sender {
    
    if ([_selectedCellName isEqualToString:@"Enter Your Name"])
    {
        
        if (![_textField.text isEqualToString:@""]) {
            NSString *urlStirn =[NSString stringWithFormat:@"http://httest.in/ftvws/RUC_ProfileUpdation/customer_id/%@/first_name/%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_id"],[_textField text]];
            [self changingTheValue:urlStirn];
        }else
        {
            UIAlertController *aler = [UIAlertController alertControllerWithTitle:@"No Text" message:@"Enter Data" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
            [aler addAction:action];
            [self presentViewController:aler animated:YES completion:nil];
        }
        
    }else if ([_selectedCellName isEqualToString:@"Enter Yur Email"])
    {
        if (![_textField.text isEqualToString:@""])
        {
            NSString *urlStirn =[NSString stringWithFormat:@"http://httest.in/ftvws/RUC_ProfileUpdation/customer_id/%@/email/%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_id"],[_textField text]];
            [self changingTheValue:urlStirn];
        }else{
            UIAlertController *aler = [UIAlertController alertControllerWithTitle:@"No Text" message:@"Enter Data" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
            [aler addAction:action];
            [self presentViewController:aler animated:YES completion:nil];
        }
       
        
    }else if ([_selectedCellName isEqualToString:@"Enter Your Number"])
    {
        if (![_textField.text isEqualToString:@""]) {
            NSString *urlStirn =[NSString stringWithFormat:@"http://httest.in/ftvws/RUC_ProfileUpdation/customer_id/%@/mobile/%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_id"],[_textField text]];
            [self changingTheValue:urlStirn];
        }else{
            UIAlertController *aler = [UIAlertController alertControllerWithTitle:@"No Text" message:@"Enter Data" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
            [aler addAction:action];
            [self presentViewController:aler animated:YES completion:nil];
        }
        
    }
    
}

-(void)changingTheValue:(NSString *)str
{
    NSLog(@"URL String is %@",str);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str]];
    NSURLSessionConfiguration *cnfgrtion = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:cnfgrtion];
    
    _dataTask = [[NSURLSessionDataTask alloc]init];
    
    _dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error)
        {
            NSMutableDictionary *dataDic2 = [[NSMutableDictionary alloc]init];
            dataDic2 = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            NSLog(@"data in dictionary is %@",dataDic2);
            
            NSString *response1 =  [NSString stringWithFormat:@"%@",[dataDic2 objectForKey:@"response"]];
            
            if (![response1 isEqualToString:@"0"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self dismissViewControllerAnimated:YES completion:nil];
                });
            }else
            {
                //Do nothing
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                });
                
            }
        }else{
            NSLog(@"Error Occured");
        }
    }];
    
    [_dataTask resume];
}

- (IBAction)cancelBtn:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
