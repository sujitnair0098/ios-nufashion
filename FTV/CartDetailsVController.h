//
//  CartDetailsVController.h
//  FTV
//
//  Created by HelixTech-Admin on 19/08/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CartTableViewCell.h"
#import "PayU_iOS_SDK.h"

@interface CartDetailsVController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSDictionary *hashDict;

@end
