//
//  Settings3TViewCell.h
//  FTV
//
//  Created by HelixTech-Admin on 09/08/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Settings3TViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *userName;

@end
