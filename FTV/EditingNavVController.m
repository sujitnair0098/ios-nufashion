//
//  EditingNavVController.m
//  FTV
//
//  Created by HelixTech-Admin on 10/08/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import "EditingNavVController.h"

@interface EditingNavVController ()

@end

@implementation EditingNavVController

- (void)viewDidLoad {
    [super viewDidLoad];
    EditingViewController *edit = [self.storyboard instantiateViewControllerWithIdentifier:@"EditingViewController"];
    [self.navigationItem setTitle:_selectedName];
    edit.selectedCellName = _selectedName;
    [self pushViewController:edit animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
