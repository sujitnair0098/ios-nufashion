//
//  Setting2TableViewCell.h
//  FTV
//
//  Created by HelixTech-Admin on 09/08/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Setting2TableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *streetName;
@property (strong, nonatomic) IBOutlet UILabel *cityName;
@property (strong, nonatomic) IBOutlet UILabel *stateName;
@property (strong, nonatomic) IBOutlet UILabel *countryName;
@property (strong, nonatomic) IBOutlet UILabel *pincode;

@end
