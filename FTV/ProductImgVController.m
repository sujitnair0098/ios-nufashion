//
//  ProductImgVController.m
//  FTV
//
//  Created by HelixTech-Admin on 10/08/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import "ProductImgVController.h"

@interface ProductImgVController ()
{
    CGFloat lastScale;
}
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property UITapGestureRecognizer *tapGesture;
@property UIPinchGestureRecognizer *pinchGesture;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *doubleTap;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

-(void)handleDoubleTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer;

@end

@implementation ProductImgVController
- (IBAction)doubleTap:(UITapGestureRecognizer *)sender {
    NSLog(@"Tap Gesture is Working");
    
    float newScale = [self.scrollView zoomScale] * 4.0;
    
    if(self.scrollView.zoomScale > self.scrollView.minimumZoomScale)
    {
        [self.scrollView zoomToRect:_imgView.frame animated:YES];
    }
    else{

        CGRect zoomRect = [self zoomRectForScale:newScale
                                      withCenter:[sender locationInView:sender.view]];
        [self.scrollView zoomToRect:zoomRect animated:YES];
        
    }
}
- (IBAction)pinchGesture:(UIPinchGestureRecognizer *)sender {

    
    if([sender state] == UIGestureRecognizerStateBegan) {
        // Reset the last scale, necessary if there are multiple objects with different scales
        lastScale = [sender scale];
    }
    
    if ([sender state] == UIGestureRecognizerStateBegan ||
        [sender state] == UIGestureRecognizerStateChanged) {
        
        CGFloat currentScale = [[[sender view].layer valueForKeyPath:@"transform.scale"] floatValue];
        
        // Constants to adjust the max/min values of zoom
        const CGFloat kMaxScale = 2.0;
        const CGFloat kMinScale = 1.0;
        
        CGFloat newScale = 1 -  (lastScale - [sender scale]); // new scale is in the range (0-1)
        newScale = MIN(newScale, kMaxScale / currentScale);
        newScale = MAX(newScale, kMinScale / currentScale);
        CGAffineTransform transform = CGAffineTransformScale([[sender view] transform], newScale, newScale);
        [sender view].transform = transform;
        self.scrollView.delegate = self;
        self.scrollView.contentSize = CGSizeMake(1280, 960);
        
        lastScale = [sender scale];  // Store the previous scale factor for the next pinch gesture call
    }
    
}

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {
    
    CGRect zoomRect;
    
    zoomRect.size.height = [_imgView frame].size.height / scale;
    zoomRect.size.width  = [_imgView frame].size.width  / scale;
    
    center = [_imgView convertPoint:center fromView:self.view];
    
    zoomRect.origin.x    = center.x - ((zoomRect.size.width / 2.0));
    zoomRect.origin.y    = center.y - ((zoomRect.size.height / 2.0));
    
    return zoomRect;
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imgView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_imgView setImage:_image];
}

-(void)handleDoubleTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer
{
    NSLog(@"Double Tapped");
    CGSize newSize = CGSizeMake(100.0, 100.0);
    if (_imgView.frame.size.width == self.view.frame.size.width) {
        newSize.width = self.view.frame.size.width*2;
        newSize.height = self.view.frame.size.height*2;
    }
    
    CGPoint currentCenter = _imgView.center;
    
    _imgView.frame = CGRectMake(_imgView.frame.origin.x, _imgView.frame.origin.y, newSize.width, newSize.height);
    _imgView.center = currentCenter;
}

- (IBAction)backBtn:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
