//
//  EditingAddrsVController.h
//  FTV
//
//  Created by HelixTech-Admin on 10/08/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface EditingAddrsVController : UIViewController<UITextFieldDelegate,CLLocationManagerDelegate>

@end
