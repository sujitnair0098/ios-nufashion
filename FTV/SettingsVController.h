//
//  SettingsVController.h
//  FTV
//
//  Created by HelixTech-Admin on 09/08/16.
//  Copyright © 2016 HelixTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Settings3TViewCell.h"
#import "Setting1TableViewCell.h"
#import "Setting2TableViewCell.h"
#import "EditingViewController.h"
#import "EditingNavVController.h"
#import "EditingAddrsVController.h"

@interface SettingsVController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property NSString *name;

@end
